const employees = require('./1-arrays-jobs.cjs');
const sumOfSalariesBasedOnCountry = require('./sumOfAllSalariesBasedOnCountries.cjs');
let averageSalaryBasedOnCountry = {};
let numOfEmployeePerCountry = {};

employees.map((employee) =>{
    (numOfEmployeePerCountry[employee.location]===undefined)?
        numOfEmployeePerCountry[employee.location] = 1:
        numOfEmployeePerCountry[employee.location]++;
})

Object.keys(sumOfSalariesBasedOnCountry).map((country)=>{
    averageSalaryBasedOnCountry[country] = sumOfSalariesBasedOnCountry[country]/numOfEmployeePerCountry[country];
})

console.log(averageSalaryBasedOnCountry);